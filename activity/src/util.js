function div_check(n) {
    if (typeof n !== "number") return false;

    return n % 5 === 0 || n % 7 === 0;
}

function factorial(n) {
    if (typeof n !== "number") return undefined;
    if (n < 0) return undefined;
    if (n === 0 || n === 1) return 1;
    return n * factorial(n - 1);
}

module.exports = {
    div_check: div_check,
    factorial: factorial
};
